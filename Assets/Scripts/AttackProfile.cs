﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackTypes
{
    platformer, normal
}

[CreateAssetMenu]
public class AttackProfile : ScriptableObject
{
    public GameObject attackGameobject;

    public AnimationClip attackAnimation;

    public AttackTypes attackType; 

}
