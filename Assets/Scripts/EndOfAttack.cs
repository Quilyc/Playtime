﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfAttack : MonoBehaviour
{
    public bool isAttackDone = false;

    public void EndAttack()
    {
        isAttackDone = true;
    }
}
