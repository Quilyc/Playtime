﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAtkHazard : MonoBehaviour
{
    [SerializeField] int hazardDamage = 1;

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.GetComponent<PlayerStats>())
        {
            other.GetComponent<PlayerStats>().TakeDmg(hazardDamage);    
        }

    }

}
