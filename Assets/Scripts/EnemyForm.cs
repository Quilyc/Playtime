﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyForm : MonoBehaviour
{
    public int enemyHp = 100;

    public int enemyMaxHp = 100;

    public Slider enemyHpSlider;

    private void Update()
    {
        
    }

    private void Start()
    {
        enemyHpSlider.maxValue = enemyMaxHp;
        UpdateEnemyHpSlider();
    }

    public void EnemyTakeDamage(int dmgReceived)
    {
        enemyHp = enemyHp - dmgReceived;
        if(enemyHp == 0)
        {
            SceneManager.LoadScene("WinScreen");
        }

        UpdateEnemyHpSlider();



    }

    void UpdateEnemyHpSlider()
    {
        enemyHpSlider.value = enemyHp;
    }


}
