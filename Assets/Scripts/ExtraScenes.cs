﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExtraScenes : MonoBehaviour
{
    public void LoadMainScene()
    {
        SceneManager.LoadScene("MainScreen");
    }

    public void ExitGame()
    {
        Application.Quit();
    }


}
