﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightController : MonoBehaviour
{

    public GameObject attackConstraints;

    public Button targetButton;

    public GameObject slidingButton;

    public GameObject spinningButton;

    public GameObject shapeButton;

    public GameObject EnemySprite;

    public Canvas gameCanvas;   

    public bool targetSpawned;

    Vector3 targetPosition;

    public Button spawnedButton;

    public Image minBorder;

    public Image maxBorder;

    float minBorderX;

    float minBorderY;

    float maxBorderX;

    float maxBorderY;

    public int ExorcismTime = 300;

    public int maxExorcismTime = 300;

    public bool ExorcismTimerOn = false;

    public bool ExorcismFinished = false;

    public GameObject ExorcismTimerText;

    private void Start()
    {
        minBorderX = minBorder.GetComponent<RectTransform>().transform.position.x;

        minBorderY = minBorder.GetComponent<RectTransform>().transform.position.y;

        maxBorderX = maxBorder.GetComponent<RectTransform>().transform.position.x;

        maxBorderY = maxBorder.GetComponent<RectTransform>().transform.position.y;
    }

    private void FixedUpdate()
    {

        if (ExorcismTimerOn == true && GetComponent<TurnHandler>().state == BattleState.Fight)
        {
            ExorcismTime--;
            
            ExorcismTimerText.SetActive(true);
            ExorcismTimerText.GetComponent<Text>().text = "" + ExorcismTime / 60;
        }

        if(ExorcismTime == 0)
        {
            ExorcismTimerOn = false;
            ExorcismFinished = true;
            ExorcismTime = maxExorcismTime;
            ExorcismTimerText.SetActive(false);
            GameObject[] toDestroy = GameObject.FindGameObjectsWithTag("Exorcism");

            for(int i = 0; i < toDestroy.Length; i++)
            {
                Destroy(toDestroy[i]);
            }
        }

    }


    public void SpawnTarget()
    {

        while (!targetSpawned)
        {
            Vector3 targetPosition = new Vector3(Random.Range(minBorderX, maxBorderX), Random.Range(minBorderY, maxBorderY), 0f);

            spawnedButton = Instantiate<Button>(targetButton, gameCanvas.transform);

            spawnedButton.transform.position = targetPosition;

            //This Doesn't work
            EnemySprite.transform.position = (Camera.main.ScreenToWorldPoint(spawnedButton.transform.position));

          



            targetSpawned = true;


        }


    }

    



}
