﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class FormProfile : ScriptableObject
{
    public string formName;

    public AttackProfile[] attackProfiles;

    public Sprite formSprite;

    public int formHp;
  
}
