﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Mode { PLATFORMER, FREE, SHOOTER, INVERSE};

public class PlayerMovement : MonoBehaviour
{

    int moveSpeed = 5;

    Rigidbody2D playerCons;

    public Mode playerMode = Mode.FREE;

    public float jumpForce;

    public float jumpVelocity;


    public Transform feetPos;

    public float checkRadius;

    bool isGrounded;

    public LayerMask whatIsGround;

    float jumpTimeCounter;

    public float jumpTime;

    bool isJumping;

    AudioSource soundEffects;

    public AudioClip staticSound;







    // Start is called before the first frame update
    void Start()
    {

        playerCons = GetComponent<Rigidbody2D>();

        soundEffects = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {

        
        //Gravity Section

       

        if(playerMode == Mode.PLATFORMER)
        {
            isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround  );
        }
        if(playerMode == Mode.PLATFORMER)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                playerCons = GetComponent<Rigidbody2D>();
                playerCons.velocity = new Vector2(playerCons.velocity.x, jumpVelocity);
            }
            
        }


    }


    private void FixedUpdate()
    {

        if (playerMode == Mode.PLATFORMER)
        {
            if (Input.GetKey(KeyCode.D))
            {

                transform.position += transform.right * (Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.A))
            {

                transform.position -= transform.right * (Time.deltaTime * moveSpeed);

           
            }

            /*
            if (Input.GetKeyDown(KeyCode.Space))
            {
                playerCons = GetComponent<Rigidbody2D>();

                playerCons.AddForce(new Vector2(0, moveSpeed), ForceMode2D.Force);
            }
            

            
            if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
            {
                isJumping = true;
                jumpTimeCounter = jumpTime;
                playerCons.velocity = Vector2.up * jumpForce;
            }
            

            
            if (Input.GetKey(KeyCode.Space) && isJumping == true )
            {
                if (jumpTimeCounter > 0)
                {
                    playerCons.velocity = Vector2.up * (jumpForce *.7f);    
                    jumpTimeCounter -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;
                }
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                isJumping = false;  
            }
            */


        }


        if (playerMode == Mode.FREE)
        {
            if (Input.GetKey(KeyCode.D))
            {

                transform.position += transform.right * (Time.deltaTime * moveSpeed);


            }

            if (Input.GetKey(KeyCode.W))
            {

                transform.position += transform.up * (Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.A))
            {

                transform.position -= transform.right * (Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.S))
            {

                transform.position -= transform.up * (Time.deltaTime * moveSpeed);
            }


            
        }

    }

    public void SetPlayer()
    {
        transform.position = new Vector2(.4f, -1f);
    }


    public void PlatformerSwitch()
    {
        playerCons.gravityScale = 10.0f;

        playerMode = Mode.PLATFORMER;

        soundEffects.clip = staticSound;

        soundEffects.Play();

        GetComponent<SpriteRenderer>().flipY = true;
    }

    public void FreeSwitch()
    {
        playerCons.gravityScale = 0f;

        playerMode = Mode.FREE;

        soundEffects.clip = staticSound;

        soundEffects.Play();

        GetComponent<SpriteRenderer>().flipY = false;
    }



}
