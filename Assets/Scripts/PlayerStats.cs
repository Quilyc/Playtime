﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    public Slider soulMeter;

    public int hp ;

    public int maxHp;

    public AudioSource soundEffects;

    public AudioClip takeDamageSound;

    public AudioClip raiseSoulSound;

    public int soulValue ;

    public int soulValueMax;



    public GameObject[] healthCrosses;



    // Start is called before the first frame update
    void Start()
    {
        hp = 3;
        maxHp = 3;
        soulValue = 100;
        soulValueMax = 100;

        soulMeter.maxValue = soulValueMax;
        soulMeter.value = soulValue;    
        soundEffects = GetComponent<AudioSource>();
        UpdateHealthUI();
    }

    // Update is called once per frame

    void Update()
    {
        
    }


    public void TakeDmg(int dmg)
    {

        hp = hp - dmg;

        UpdateHealthUI();
      
        soundEffects.clip = takeDamageSound;
        soundEffects.Play();


        if (hp <= 0)
        {
            GameOver();
        }
    }

    public void AdjustSoul( int amt )
    {
        
        soulValue += amt;

        if (soulValue > 100)
        {
            soulValue = 100;
        }
        
        soulMeter.value = soulValue;
        soundEffects.clip = raiseSoulSound;
        soundEffects.Play();
     

    }

    void UpdateHealthUI()
    {
        switch (hp)
        {
            case 1:
                healthCrosses[0].SetActive(true);
                healthCrosses[1].SetActive(false);
                healthCrosses[2].SetActive(false);
                healthCrosses[3].SetActive(false);

                break;

            case 2:
                healthCrosses[0].SetActive(true);
                healthCrosses[1].SetActive(true);
                healthCrosses[2].SetActive(false);
                healthCrosses[3].SetActive(false);
                break;

            case 3:
                healthCrosses[0].SetActive(true);
                healthCrosses[1].SetActive(true);
                healthCrosses[2].SetActive(true);
                healthCrosses[3].SetActive(false);

                break;

            case 4:
                healthCrosses[0].SetActive(true);
                healthCrosses[1].SetActive(true);
                healthCrosses[2].SetActive(true);
                healthCrosses[3].SetActive(true);

                break;
        }

    }

    void GameOver()
    {
        SceneManager.LoadScene("GameOverScreen");
    }



    

}
