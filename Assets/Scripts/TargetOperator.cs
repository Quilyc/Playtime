﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetOperator : MonoBehaviour
{
    enum ButtonTypes
    {
        normal, sliding
    }

    [SerializeField] ButtonTypes buttonType;
    [SerializeField] int normTargetDmg;
    Button myButton;

    GameObject GameController;

    private void Start()
    {
        myButton = GetComponent<Button>();
        if (buttonType == ButtonTypes.normal)
        {
            myButton.onClick.AddListener(OnNormButtonPress);
        }
        GameController = GameObject.FindGameObjectWithTag("GameController");


      
    }

    void OnNormButtonPress()
    {
        GameObject enemyToDamage = GameObject.FindGameObjectWithTag("EnemyBody");

        enemyToDamage.GetComponent<EnemyForm>().EnemyTakeDamage(normTargetDmg);

        GameController.GetComponent<AudioSource>().Play();

        GameController.GetComponent<FightController>().targetSpawned = false;

        Destroy(gameObject);
    }


    
}
