﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BattleState
{
    Start, PlayerTurn, EnemyTurn, FinishedTurn, Won, Lost, Fight
}


public class TurnHandler : MonoBehaviour
{

    public BattleState state;

    public FormProfile currentForm;

    private bool enemyActed = false;

    public PlayerMovement playerMovement;

    public PlayerStats playerStats;

    public GameObject player;

    Animator animationComponent;

    bool attacksCreated = false;

    bool attackon = false;

    GameObject cAttackGameObject;

    public GameObject PrayButton;

    public GameObject ExorciseButton;

    public GameObject ItemButton;

    public GameObject EnemySprite;

    public GameObject box;

    public EnemyForm currentEnemy;

    public Vector3 enemyTransform;

    AttackProfile currentAttack;
   

    void Start()
    {
        state = BattleState.Start;


    }

    
    void Update()
    {
        if(state == BattleState.Start)
        {
            //currentEnemy.enemyHp = currentForm.formHp;

            //currentEnemy.enemyMaxHp = currentForm.formHp;

            state = BattleState.PlayerTurn;
        }


        else if(state == BattleState.PlayerTurn)
        {
            SetupButtons("start");



        }

        else if(state == BattleState.EnemyTurn)
        {

            //Subject to Change this enemy position

            EnemySprite.transform.position = enemyTransform;
            
            if(attacksCreated == false)
            {
                playerMovement.SetPlayer();

                int currentAttackNum = Random.Range(0, currentForm.attackProfiles.Length);

                currentAttack = currentForm.attackProfiles[currentAttackNum];

                if (currentAttack.attackType == AttackTypes.platformer)
                {
                    playerMovement.PlatformerSwitch();
                }

                cAttackGameObject = Instantiate(currentAttack.attackGameobject, currentAttack.attackGameobject.transform.position, Quaternion.identity);

                animationComponent = cAttackGameObject.GetComponent<Animator>();

                animationComponent.Play(currentAttack.attackAnimation.name);

                attacksCreated = true;

                
            }

            if(cAttackGameObject.GetComponent<EndOfAttack>().isAttackDone == true)
            {
                Destroy(cAttackGameObject);

                attacksCreated = false;

                state = BattleState.PlayerTurn;

                if(currentAttack.attackType == AttackTypes.platformer)
                {
                    playerMovement.FreeSwitch();
                }
              

               
            }






               
        }
        
        else if(state == BattleState.Fight)
        {
            DisableField();
            GetComponent<FightController>().SpawnTarget();
            GetComponent<FightController>().ExorcismTimerOn = true;

            if(GetComponent<FightController>().ExorcismFinished == true)
            {
                state = BattleState.EnemyTurn;
                playerStats.AdjustSoul(-100);
                GetComponent<FightController>().ExorcismFinished = false;
                EnableField();


            }


        }

   

        


    }

    public void Pray()
    {
        playerStats.AdjustSoul(20);
        PlayerFinishTurn();
    }

    public void Exorcise()
    {
        state = BattleState.Fight;
        GetComponent<FightController>().targetSpawned = false;
    }

    public void PlayerFinishTurn()
    {
        SetupButtons("stop");

        state = BattleState.EnemyTurn;

    }

    public void SetupButtons(string command)
    {
        if(command == "start")
        {
            if (playerStats.soulValue == 100)
            {
                PrayButton.SetActive(false);
                ExorciseButton.SetActive(true);
            }
            else
            {
                PrayButton.SetActive(true);
                ExorciseButton.SetActive(false);
            }
        }

        if(command == "stop")
        {
            PrayButton.SetActive(false);
            ExorciseButton.SetActive(false);
        }
        
    }

    void DisableField()
    {
        PrayButton.SetActive(false);
        ExorciseButton.SetActive(false);
        player.SetActive(false);
        box.SetActive(false);
    }

    void EnableField()
    {
        
        player.SetActive(true);
        box.SetActive(true);
    }

    
}
